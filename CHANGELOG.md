## [2.3.1](https://gitlab.com/roivant/oss/workstation-setup/compare/v2.3.0...v2.3.1) (2023-09-22)


### Fix

* ordering of mac scripts, global vars, python linking and iterm defaults ([b7a8006](https://gitlab.com/roivant/oss/workstation-setup/commit/b7a8006438a32bb2a7f87f63b034dcd5cb2b7df4))

# [2.3.0](https://gitlab.com/roivant/oss/workstation-setup/compare/v2.2.0...v2.3.0) (2023-09-22)


### New

* fixed up setup.ps1 plus semantic-release ([868327f](https://gitlab.com/roivant/oss/workstation-setup/commit/868327f777fe52b63c575132cce92b0121710e57))

# [2.2.0](https://gitlab.com/roivant/oss/workstation-setup/compare/v2.1.0...v2.2.0) (2023-04-19)


### Fix

* missing bash? ([a67acfa](https://gitlab.com/roivant/oss/workstation-setup/commit/a67acfa8fddb3aa95627c3b6b48f748a33f12625))
* move some ci bits around ([c2d90c2](https://gitlab.com/roivant/oss/workstation-setup/commit/c2d90c2d0978e397501fbf0db8fbba35d0d206ae))

### New

* more support for moving from vscode to codium ([e3fb30a](https://gitlab.com/roivant/oss/workstation-setup/commit/e3fb30ae50ba2ecf5c51b5742b7b09f2fbdde22c))

# [2.1.0](https://gitlab.com/roivant/oss/workstation-setup/compare/v2.0.2...v2.1.0) (2023-04-18)


### Fix

* designer tools, vcodium install, misc ([90e85dc](https://gitlab.com/roivant/oss/workstation-setup/commit/90e85dcf9862d0ab030934f006c5440e2bc56961))
* robust gitignore ([b74c0c2](https://gitlab.com/roivant/oss/workstation-setup/commit/b74c0c25106592a32ccd77a1520989499aaea83a))
* source helpers universally. standardize options ([107790e](https://gitlab.com/roivant/oss/workstation-setup/commit/107790e3ce19b3f894db43f9b19de911828b541e))

### New

* Revamped the start script to not be exclusively roivant ([37d0540](https://gitlab.com/roivant/oss/workstation-setup/commit/37d0540be525cdfd0b23f54b5123da8618593518))
* semantic-release ([0ba26f8](https://gitlab.com/roivant/oss/workstation-setup/commit/0ba26f82efdb8dae9993dd3ac00e7e06eeb431ae))
* Substantial updates ([7c280c1](https://gitlab.com/roivant/oss/workstation-setup/commit/7c280c118fae95bdb396088da4f7091a9b5dd301))
