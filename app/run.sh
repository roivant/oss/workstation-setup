#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

osascript runinterminal.scpt "\"${SCRIPT_DIR}/setup.sh\""
