#!/bin/bash

if ! [ -e "/Applications/Platypus.app/Contents/MacOS/Platypus" ]; then
    echo "ERROR: I can't find the Platypus executable."
    echo "Please make sure you have downloaded it from here: http://sveinbjorn.org/platypus"
    echo "And added it to /Applications"
    exit 1
fi

cat app/Workstation\ Setup.platypus.template | sed -e "s#%HOME%#${HOME}#g" -e "s#%PWD%#${PWD}#g" > Workstation\ Setup.platypus

if ! [ -e /usr/local/bin/platypus ]; then
    echo "Platypus command line tool isn't installed.  Open Platypus, go to preferences and click the install button"
else
    /usr/local/bin/platypus -P "$PWD/Workstation Setup.platypus" -y "$PWD/app/Workstation Setup.app"
fi
