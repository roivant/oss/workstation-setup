#!/usr/bin/env bash

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
VERS="$(git describe --tags --abbrev=0)"

mkdir -p dist

cd ../
rm -f workstation-setup/dist/workstation-setup-repo-${VERS}.tar.gz
tar --exclude="workstation-setup/dist" -zcf workstation-setup/dist/workstation-setup-repo-${VERS}.tar.gz workstation-setup

#cd "$HERE"
#rm -f dist/workstation-setup-app-${VERS}.zip
#zip -qr dist/workstation-setup-app-${VERS}.zip app/Workstation\ Setup.app
#echo
#rm -rf Workstation\ Setup.platypus

