pushd ~/workspace
if [ ! -d ~/workspace/jetbrains-ide-prefs ]; then
    green "Downloading JetBrains IDE preferences"
    git clone https://github.com/professor/jetbrains-ide-prefs.git
else
    cd ~/workspace/jetbrains-ide-prefs
    git pull
fi
popd
