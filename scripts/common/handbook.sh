#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

if [[ $isroi -eq 1 ]]; then

if `git ls-remote git@gitlab.com:roivant/vant-it/roivant-it-handbook.git >/dev/null 2>&1`; then
purple '
Setting up the...
██╗████████╗    ██╗  ██╗ █████╗ ███╗   ██╗██████╗ ██████╗  ██████╗  ██████╗ ██╗  ██╗
██║╚══██╔══╝    ██║  ██║██╔══██╗████╗  ██║██╔══██╗██╔══██╗██╔═══██╗██╔═══██╗██║ ██╔╝
██║   ██║       ███████║███████║██╔██╗ ██║██║  ██║██████╔╝██║   ██║██║   ██║█████╔╝ 
██║   ██║       ██╔══██║██╔══██║██║╚██╗██║██║  ██║██╔══██╗██║   ██║██║   ██║██╔═██╗ 
██║   ██║       ██║  ██║██║  ██║██║ ╚████║██████╔╝██████╔╝╚██████╔╝╚██████╔╝██║  ██╗
╚═╝   ╚═╝       ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═════╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚═╝  ╚═╝
'

    # *Ensure* the workspace folder exists
    mkdir -p ~/workspace
    cd ~/workspace

    if [ -e "roivant-it-handbook" ]; then
        green "Handbook directory exists, updating"
        cd roivant-it-handbook
        git pull
    else
        green "Cloning the Handbook repository"
        git clone git@gitlab.com:roivant/vant-it/roivant-it-handbook.git
        cd roivant-it-handbook
    fi

    branchname=`echo $USERNAME | sed -e "s/ /_/g" -e "s/\./_/g"`

    green "Checking for personal git branch"

    if `git branch --list $branchname >/dev/null`; then
        green "Branch $branchname exists! w00t."
        git checkout $branchname
    else
        green "Creating new branch: $branchname"
        git checkout main
        git pull
        git submodule update --init --recursive
        git checkout -b $branchname
        git push --set-upstream origin $branchname
    fi

else
    redb "Git handbook access isn't working (could not ls-remote), you'll 
    need to check this manually"
fi

fi
