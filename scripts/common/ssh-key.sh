MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

iam=`whoami | sed -e "s/ /_/g"`
mkdir -p ~/.ssh

if ! [ -e ~/.ssh/id_ed25519.pub ]; then
    echo "Creating an SSH key"
    ssh-keygen -t ed25519 -N "" -C "${iam}@roivant" -f ~/.ssh/id_ed25519 2>&1 >/dev/null
fi

