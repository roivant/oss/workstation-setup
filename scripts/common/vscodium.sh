#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

if [ "$PLATFORM" == "mac" ]; then

purple '
Setting up...
██╗   ██╗███████╗ ██████╗ ██████╗ ██████╗ ██╗██╗   ██╗███╗   ███╗
██║   ██║██╔════╝██╔════╝██╔═══██╗██╔══██╗██║██║   ██║████╗ ████║
██║   ██║███████╗██║     ██║   ██║██║  ██║██║██║   ██║██╔████╔██║
╚██╗ ██╔╝╚════██║██║     ██║   ██║██║  ██║██║██║   ██║██║╚██╔╝██║
 ╚████╔╝ ███████║╚██████╗╚██████╔╝██████╔╝██║╚██████╔╝██║ ╚═╝ ██║
  ╚═══╝  ╚══════╝ ╚═════╝ ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝
'

    cask "/Applications/VSCodium.app" vscodium

    CODIUM="/Applications/VSCodium.app/Contents/Resources/app/bin/codium"

    ln -sf "$CODIUM" $(brew --prefix)/bin/codium
    ln -sf "$CODIUM" $(brew --prefix)/bin/code

    if ! [ -e "${HOME}/Library/Application Support/VSCodium/product.json" ]; then
        cp "${WS_DIR}/files/product.json" "${HOME}/Library/Application Support/VSCodium/product.json"
    fi

    

else

cat << EOF
##     ##  ######   ######   #######  ########  #### ##     ## ##     ## 
##     ## ##    ## ##    ## ##     ## ##     ##  ##  ##     ## ###   ### 
##     ## ##       ##       ##     ## ##     ##  ##  ##     ## #### #### 
##     ##  ######  ##       ##     ## ##     ##  ##  ##     ## ## ### ## 
 ##   ##        ## ##       ##     ## ##     ##  ##  ##     ## ##     ## 
  ## ##   ##    ## ##    ## ##     ## ##     ##  ##  ##     ## ##     ## 
   ###     ######   ######   #######  ########  ####  #######  ##     ## 
EOF

    choco install vscodium

    if ! [ -e "${HOME}/AppData/Roaming/VSCodium/product.json" ]; then
        cp "${WS_DIR}/files/product.json" "${HOME}/AppData/Roaming/VSCodium/product.json"
    fi

    CODIUM="/c/Program Files/VSCodium/bin/codium"

fi

lightblueb "Copying data and settings from VScode (if they exist)"

# These directories are actually cross-platform
mkdir -p ~/.vscode-oss

# Copy all extensions
if [ -e "~/.vscode/extensions" ]; then
    cp -a ~/.vscode/extensions ~/.vscode-oss/
fi

# Copy keybindings.json and settings.json files
# "||:" suppresses error return because it's ok if the source file doesn't exist
if [ "$PLATFORM" == "mac" ]; then

    mkdir -p ~/Library/Application\ Support/VSCodium/User
    
    cp ~/Library/Application\ Support/Code/User/keybindings.json ~/Library/Application\ Support/VSCodium/User 2>/dev/null || :
    cp ~/Library/Application\ Support/Code/User/settings.json ~/Library/Application\ Support/VSCodium/User 2>/dev/null || :

else

    mkdir -p $APPDATA/Code/User

    cp $APPDATA/Code/User/settings.json  $APPDATA/VSCodium/User/ 2>/dev/null || :
    cp $APPDATA/Code/User/keybindings.json  $APPDATA/VSCodium/User/ 2>/dev/null || :

fi

"$CODIUM" -v 2>&1 >/dev/null

if [ $? -ne 0 ]; then
    red "
    OH NOES. The 'codium' command doesn't seem to be working. You may need to complete
    this manually.  Here are the extensions you need to install:"

    yellow $extensions

else

    installed=$("$CODIUM" --list-extensions)
    extensions=$(cat "$WS_DIR/files/vs-extensions.txt")

    green "Installing vscode extensions"

    for ext in $extensions; do
        if `echo "$installed" | grep -qi "$ext"`; then
            lightblue "### ${ext} already installed" nobreaks
        else
            lightblue "### Installing ${ext}"
            python "$WS_DIR/scripts/common/download-vsix.py" $ext
            "$CODIUM" --install-extension ${ext}.vsix
            rm -f ${ext}.vsix
        fi
    done

    

    if [[ $PLATFORM == "mac" ]]; then
        cfg_path="${HOME}/Library/Application Support/VSCodium/User/settings.json"
    else
        cfg_path="${APPDATA}/VSCodium/User/settings.json"
    fi

    if ! [ -e "$cfg_path" ]; then
        if [ -e "$APPDATA/Code/User/settings.json" ]; then
            jq -s '.[0] * .[1]' "$WS_DIR/files/settings.json" "$APPDATA/Code/User/settings.json" > "$cfg_path"
        else
            cp "$WS_DIR/files/settings.json" "$cfg_path"
        fi
    else
        green "VScodium \"$cfg_path\" already exists, I will not overwrite it"
    fi

fi