
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

check_os=`uname -s`

case $check_os in 
    MSYS*|MINGW*)
        PLATFORM="windows"
        ;;
    Darwin)
        PLATFORM="mac"
        ;;
esac

if [ "$PLATFORM" == "mac" ]; then
    if [ -e /usr/bin/python ]; then
        PYTHON="/usr/bin/python"
    elif [ -e /usr/bin/python3 ]; then
        PYTHON="/usr/bin/python3"
    else
        echo "OMG NO PYTHON. srsly I can't continue"
        exit
    fi
else
    # If we're on windows, this should be in the path already
    PYTHON="python"
fi

WS_DIR=$(realpath "${MY_DIR}/../..")

ARCH=`uname -m`

if [[ "$ARCH" == 'arm64' ]]; then
    HB_PATH="/opt/homebrew"
else
    HB_PATH="/usr/local"
fi

export ARCH
export HB_PATH
export PLATFORM
export PYTHON
export WS_DIR

########################################
# HB cask installs
########################################
function cask() {

    dest="$1"
    app="$2"
    
    if [ -e "$dest" ]; then
        green "${app} already installed"
    else
        brew install --cask ${app}
    fi

}

########################################
# Append a line to a file only if a 
# string does not exist in it
########################################
function addline() {


    line="$1"
    file="$2"
    checkfor="$3"

    if [ -e "$file" ]; then
        # If the line or check-string already exists in the file,
        # just return
        if [ -z "$checkfor" ]; then
            if `grep -q "$line" "$file"`; then
                return
            fi
        else
            if `grep -q "$checkfor" "$file"`; then
                return
            fi
        fi
    fi
    # ensure the directory path exists
    mkdir -p "`dirname $file`"
    echo "$line" >> "$file"
}


########################################
# Text output
########################################
function output {
    if [ "$2" = "nobreaks" ]; then
        msg="$1"
    else
        msg="\n$1\n"
    fi
    echo -e "$msg"
}

########################################
# Colors
########################################
end="\033[0m"
black="\033[0;30m"
blackb="\033[1;30m"
white="\033[0;37m"
whiteb="\033[1;37m"
red="\033[0;31m"
redb="\033[1;31m"
green="\033[0;32m"
greenb="\033[1;32m"
yellow="\033[0;33m"
yellowb="\033[1;33m"
blue="\033[0;34m"
blueb="\033[1;34m"
purple="\033[0;35m"
purpleb="\033[1;35m"
lightblue="\033[0;36m"
lightblueb="\033[1;36m"

function black {
    output "${black}${1}${end}" ${@:2}
}

function blackb {
    output "${blackb}${1}${end}" ${@:2}
}

function white {
    output "${white}${1}${end}" ${@:2}
}

function whiteb {
    output "${whiteb}${1}${end}" ${@:2}
}

function red {
    output "${red}${1}${end}" ${@:2}
}

function redb {
    output "${redb}${1}${end}" ${@:2}
}

function green {
    output "${green}${1}${end}" ${@:2}
}

function greenb {
    output "${greenb}${1}${end}" ${@:2}
}

function yellow {
    output "${yellow}${1}${end}" ${@:2}
}

function yellowb {
    output "${yellowb}${1}${end}" ${@:2}
}

function blue {
    output "${blue}${1}${end}" ${@:2}
}

function blueb {
    output "${blueb}${1}${end}" ${@:2}
}

function purple {
    output "${purple}${1}${end}" ${@:2}
}

function purpleb {
    output "${purpleb}${1}${end}" ${@:2}
}

function lightblue {
    output "${lightblue}${1}${end}" ${@:2}
}

function lightblueb {
    output "${lightblueb}${1}${end}" ${@:2}
}

function colors {
  black "black"
  blackb "blackb"
  white "white"
  whiteb "whiteb"
  red "red"
  redb "redb"
  green "green"
  greenb "greenb"
  yellow "yellow"
  yellowb "yellowb"
  blue "blue"
  blueb "blueb"
  purple "purple"
  purpleb "purpleb"
  lightblue "lightblue"
  lightblueb "lightblueb"
}

function colortest {
  if [[ -n "$1" ]]; then
    T="$1"
  fi
  T='gYw'   # The test text

    output "\n                 40m     41m     42m     43m\
       44m     45m     46m     47m";

  for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' \
             '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' \
             '  36m' '1;36m' '  37m' '1;37m';
    do FG=${FGs// /}
      outputn " $FGs \033[$FG  $T  "
    for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
      do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
    done
    echo;
  done
  echo
}