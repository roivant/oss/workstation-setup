#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

# All these applications are independent, so if one
# fails to install, don't stop.
set +e

purple '
         Installing
 █████╗ ██████╗ ██████╗ ███████╗
██╔══██╗██╔══██╗██╔══██╗██╔════╝
███████║██████╔╝██████╔╝███████╗
██╔══██║██╔═══╝ ██╔═══╝ ╚════██║
██║  ██║██║     ██║     ███████║
╚═╝  ╚═╝╚═╝     ╚═╝     ╚══════╝ 
'

if [ "$ARCH" = "arm64" ]; then
    echo "Installing Rosetta 2"
    /usr/sbin/softwareupdate --install-rosetta --agree-to-license
fi

# Place any applications that require the user to type in their password here
cask "/Applications/zoom.us.app" zoom

# Utilities
cask "/Applications/Maccy.app" maccy

# Terminals
cask "/Applications/iTerm.app" iterm2

# Browsers
cask "/Applications/Google Chrome.app" google-chrome
cask "/Applications/Firefox.app" firefox

# Communication

cask "/Applications/Slack.app" slack
cask "/Applications/Miro.app" miro
cask "/Applications/Signal.app" signal

# Text Editors
cask "/Applications/MacDown.app" macdown
cask "/Applications/JetBrains Toolbox.app" jetbrains-toolbox

## Vim
brew install vim
brew unlink vim  # macvim won't install if vim links are in place
cask "/Applications/MacVim.app" macvim
brew link --overwrite vim # overwrites cli vim links only

# Misc
cask "/Applications/1Password.app" 1password
cask "/usr/local/bin/op" 1password-cli
cask "/Applications/Postman.app" postman
cask "/Applications/WorkSpaces.app" amazon-workspaces

brew install jq

# Primary IDE
source "${WS_DIR}/scripts/mac/vscodium.sh"

set -e
