#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

purple "Configuring those apps..."

green "> Configuring iTerm"

echo "$MY_DIR $WS_DIR"
if ! [ -e ~/Library/Preferences/com.googlecode.iterm2.plist ]; then
    green "Setting default iTerm preferences"
    cp "${WS_DIR}/files/com.googlecode.iterm2.plist" ~/Library/Preferences
fi

green "> Configuring Maccy"
open /Applications/Maccy.app

# Disabling this for now
# echo
# echo "Installing vim configuration"
# pushd ~/
# if [ ! -d ~/.vim ]; then
#     git clone https://github.com/pivotal/vim-config.git ~/.vim
#     ~/.vim/bin/install
# fi
# popd


