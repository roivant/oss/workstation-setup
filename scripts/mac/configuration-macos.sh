#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

purple 'Customizing settings for...
███╗   ███╗ █████╗  ██████╗ ██████╗ ███████╗
████╗ ████║██╔══██╗██╔════╝██╔═══██╗██╔════╝
██╔████╔██║███████║██║     ██║   ██║███████╗
██║╚██╔╝██║██╔══██║██║     ██║   ██║╚════██║
██║ ╚═╝ ██║██║  ██║╚██████╗╚██████╔╝███████║
╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝
'

DOCK_REMOVE="FaceTime|Messages|Maps|Music|Podcasts|TV|News|App|System|Feedback"

# set menu clock
# see http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Format_Patterns
defaults write com.apple.menuextra.clock "DateFormat" 'EEE MMM d  h:mm:ss a'
killall SystemUIServer

# hide the dock
defaults write com.apple.dock autohide -bool true
killall Dock

# fast key repeat rate, requires reboot to take effect
defaults write ~/Library/Preferences/.GlobalPreferences KeyRepeat -int 1
defaults write ~/Library/Preferences/.GlobalPreferences InitialKeyRepeat -int 15

# set finder to display full path in title bar
defaults write com.apple.finder '_FXShowPosixPathInTitle' -bool true

# stop Photos from opening automatically
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true
#to revert use defaults -currentHost delete com.apple.ImageCapture disableHotPlug

# modify appearance of dock: remove standard icons, add chrome and iTerm
if [ ! -e /usr/local/bin/dockutil ]; then
    wget https://github.com/kcrawford/dockutil/releases/download/3.0.2/dockutil-3.0.2.pkg
    sudo installer -pkg dockutil-3.0.2.pkg -tgt /
fi

# As of 3-31-22 dockutil forces install into /usr/local
chmod a+rx,go-w /usr/local/bin/dockutil
dockutil --list | egrep "^(${DOCK_REMOVE})" | awk -F\t '{print "dockutil --remove \""$1"\" --no-restart"}' | sh

dock_items=`dockutil --list | awk '{ split($0,a,"\t"); print a[1] }' | xargs`

for app in "Google Chrome" "iTerm" "Miro" "Slack" "VSCodium"; do
    if ! `echo "$dock_items" | grep -qi "$app"`; then
        dockutil --add "/Applications/${app}.app"
    fi
done
