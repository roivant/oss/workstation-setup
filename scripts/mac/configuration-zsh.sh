#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

purple '
Setting up...
███████╗███████╗██╗  ██╗
╚══███╔╝██╔════╝██║  ██║
  ███╔╝ ███████╗███████║
 ███╔╝  ╚════██║██╔══██║
███████╗███████║██║  ██║
╚══════╝╚══════╝╚═╝  ╚═╝
'

brew install autojump
brew install grc
brew install watch

if ! [ -e "${HOME}/.oh-my-zsh" ]; then
  whiteb "Installing Oh My Zsh"
export RUNZSH="no"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
else
  greenb "Oh My Zsh already installed"
fi

brew install direnv