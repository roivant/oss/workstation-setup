#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

purple '
 ██████╗ ██╗████████╗
██╔════╝ ██║╚══██╔══╝
██║  ███╗██║   ██║   
██║   ██║██║   ██║   
╚██████╔╝██║   ██║   
 ╚═════╝ ╚═╝   ╚═╝    
'
green "Installing Git and associated tools (homebrew versions)"
brew install git

# FIXME: 'advanced' mode for extra tools
# brew tap git-duet/tap
# brew install git-duet
# brew install git-pair
# brew install git-together
# brew install git-author
# brew install --cask rowanj-gitx --force
# brew install --cask gitup --force

# echo
# echo "Putting a sample git-pair file in ~/.pairs"
# cp files/.pairs ~/.pairs

green "Setting global Git configurations"
git config --global core.editor ${HB_PATH}/bin/vim
git config --global transfer.fsckobjects true

green "Setting default git editor to vscode."
git config --global core.editor "code --wait"

mkdir -p ~/.git_templates
git config --global init.templateDir ~/.git_templates
echo "ref: refs/heads/main" > ~/.git_templates/HEAD

