#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

purple '
██╗  ██╗ ██████╗ ███╗   ███╗███████╗██████╗ ██████╗ ███████╗██╗    ██╗
██║  ██║██╔═══██╗████╗ ████║██╔════╝██╔══██╗██╔══██╗██╔════╝██║    ██║
███████║██║   ██║██╔████╔██║█████╗  ██████╔╝██████╔╝█████╗  ██║ █╗ ██║
██╔══██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗██╔══██╗██╔══╝  ██║███╗██║
██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗██████╔╝██║  ██║███████╗╚███╔███╔╝
╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═════╝ ╚═╝  ╚═╝╚══════╝ ╚══╝╚══╝ 
'

setup_hb_rcfile() {
    if ! `grep -q HOMEBREW_PREFIX "${2}"`; then
        echo "# HOMEBREW" >> $2
        ${1}/bin/brew shellenv >> $2
    fi
}

if hash brew 2>/dev/null; then
  green "Homebrew is already installed!"
else
  green "Installing Homebrew..."
  yes '' | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

if [[ "$SHELL" == "/bin/bash" ]]; then
    setup_hb_rcfile "$HB_PATH" "$HOME/.bashrc"
elif [[ "$SHELL" == "/bin/zsh" ]]; then
    setup_hb_rcfile "$HB_PATH" "$HOME/.zprofile"
else
    echo "WARNING: Unknown shell ($SHELL). I can't set up your rc file for homebrew."
fi

eval "$(${HB_PATH}/bin/brew shellenv)"

green "Updating to ensure you have the latest Homebrew..."
brew update -v

green "Ensuring your Homebrew directory is writable..."
sudo chown -Rf $(whoami) $(brew --prefix)/*

green "Installing Homebrew services..."
brew tap homebrew/services

green "Upgrading existing brews..."
brew upgrade

green "Cleaning up your Homebrew installation..."
brew cleanup

green "Adding Homebrew's sbin to your PATH..."
echo "export PATH=\"${HB_PATH}/sbin:$PATH\"" >> ~/.bash_profile
