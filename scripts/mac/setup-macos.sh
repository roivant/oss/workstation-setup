#!/usr/bin/env bash
#
# setup-macos.sh:  Set up a macos workstation
#
# Arguments:
#   - a list of components to install, see scripts/opt-in/ for valid options
#

# Fail immediately if any errors occur
set -e

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Source helpers and colors if they haven't been already
if [ -z ${black+x} ]; then
    source ${MY_DIR}/../helpers/helpers.sh
fi

lightblue "############
Running setup for a Mac
############"

if [ $# -eq 0 ]; then

yellow "############" nobreaks
yellowb "Select optional installs here" nobreaks
yellow "See this list below this yellow section? We can install toolchains and support for these
optional things. You'll need to do some typing, or copying and pasting,
but at the prompt, type each one you want to include, separated by spaces, then
hit enter.  If you want C, Java and Go, you would type in:

c golang java

Then hit enter. 
############"

output "Available options:"

/bin/ls $MY_DIR/../opt-in/ | xargs -n1 basename -s .sh | pr -2 -t -s | column -t
echo

IFS= read -r -p "Type the options you want here: " options

else
options="$@"
fi

output "
Will install: " nobreaks
if [ -z "$options" ]; then
    whiteb "None"
else
    whiteb "$options"
fi

yellowb "Now, we have to prompt you for your computer password here, to do some administrative
operations (sudo). Yes, this is safe..."

sudo -K
sudo true;
clear

yellowb "
############
From this point on, you will see a bunch of text fly by while lots of 
things are installed and configured.  It'll take a few minutes, so feel free
to ignore this window for a while.
############
"

blue "[Hit enter to continue]" nobreaks
read

export HOMEBREW_NO_ANALYTICS=1

# Make absolutely sure the workspace folder exists
mkdir -p ~/workspace

# Note: Homebrew needs to be set up first
source "${WS_DIR}/scripts/mac/homebrew.sh"
source "${WS_DIR}/scripts/common/download-jetbrains-ide-prefs.sh"
source "${WS_DIR}/scripts/mac/applications.sh"
source "${WS_DIR}/scripts/common/configuration-bash.sh"
source "${WS_DIR}/scripts/mac/configuration-zsh.sh"
source "${WS_DIR}/scripts/mac/git.sh"
source "${WS_DIR}/scripts/common/git-aliases.sh"
source "${WS_DIR}/scripts/mac/unix.sh"
source "${WS_DIR}/scripts/mac/configuration-macos.sh"
source "${WS_DIR}/scripts/mac/configuration-apps.sh"


# For each command line argument, try executing the corresponding script in opt-in/
whiteb "Installing options"

for var in $options; do
    FILE="${WS_DIR}/scripts/opt-in/${var}.sh"
    if [ -f $FILE ]; then
        echo "$FILE"
        source "${FILE}"
    else
       echo "Warning: File $FILE does not exist."
    fi
done

source "${WS_DIR}/scripts/common/handbook.sh"
source "${WS_DIR}/scripts/common/finished.sh"