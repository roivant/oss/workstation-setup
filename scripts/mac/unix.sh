#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

echo
purple 'Installing utilities for
██╗   ██╗███╗   ██╗██╗██╗  ██╗    ████████╗██╗  ██╗██╗███╗   ██╗ ██████╗ ███████╗
██║   ██║████╗  ██║██║╚██╗██╔╝    ╚══██╔══╝██║  ██║██║████╗  ██║██╔════╝ ██╔════╝
██║   ██║██╔██╗ ██║██║ ╚███╔╝        ██║   ███████║██║██╔██╗ ██║██║  ███╗███████╗
██║   ██║██║╚██╗██║██║ ██╔██╗        ██║   ██╔══██║██║██║╚██╗██║██║   ██║╚════██║
╚██████╔╝██║ ╚████║██║██╔╝ ██╗       ██║   ██║  ██║██║██║ ╚████║╚██████╔╝███████║
 ╚═════╝ ╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝       ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝
'

# For users of unixes
brew install pstree
brew install the_silver_searcher
brew install wget
brew install coreutils

# For developers of shell scripts
brew install jq
brew install jsonpp
