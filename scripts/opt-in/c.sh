
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing common C development tools and libraries"
brew install ccache
brew install ctags
brew install cmake
brew install cscope
brew install --cask clion
brew install ninja

source ${WS_DIR}/scripts/common/download-jetbrains-ide-prefs.sh
pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=clion
popd