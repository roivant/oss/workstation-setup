
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing Cloud Foundry Command-line Interface"

brew tap cloudfoundry/tap
brew install cf-cli
brew install bosh-cli
brew install bbl
