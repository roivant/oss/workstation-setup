#!/usr/bin/env bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

# All these applications are independent, so if one
# fails to install, don't stop.
set +e

green "Installing Designer applications"

# Graphic editing tools
cask "/Applications/GIMP.app" gimp
cask "/Applications/Inkscape.app" inkscape
cask "/Applications/Krita.app" krita
cask "/Applications/Lunacy.app" lunacy

set -e
