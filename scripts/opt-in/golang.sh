
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing Golang Development tools"

mkdir -p ~/go/src
brew install go

# Don't cask goland if toolbox is installed
if ! [ -e "/Applications/JetBrains Toolbox.app" ]; then
    brew install --cask goland
else
    yellow "Not installing Goland since you have the JetBrains Toolbox"
fi

pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=goland
popd
