
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing iOS developer tools"

# package managers
sudo gem install cocoapods
brew install carthage

# deployment manager
brew install fastlane

# ide
brew install --cask appcode --force
