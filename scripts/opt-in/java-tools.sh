
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing Java Development tools"

brew install --cask intellij-idea --force # guard against pre-installed intellij
brew tap jcgay/jcgay
brew install maven-deluxe
brew install gradle
brew install springboot

pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=intellij
popd
