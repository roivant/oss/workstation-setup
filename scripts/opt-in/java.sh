
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing most recent version of Java"
brew install --cask java
source ${WS_DIR}/scripts/opt-in/java-tools.sh
