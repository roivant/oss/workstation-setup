
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing common Kubernetes tooling"

brew install kubectl
# Shell completion
FILE=~/.zshrc
if [[ -f "$FILE" ]]; then
    echo "$FILE exists proceeding."
else 
    echo "$FILE does not exist, creating."
    touch $FILE
fi

addline 'source <(kubectl completion zsh)' >>~/.zshrc
addline 'alias k=kubectl' >>~/.zshrc
addline 'complete -F __start_kubectl k' >>~/.zshrc
addline 'autoload -Uz compinit' >>~/.zshrc
addline 'compinit' >>~/.zshrc