
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing most recent version of NodeJS"

brew install node

green "Installing global NodeJS Packages"

npm install --global yo
npm install --global webpack
npm install --global grunt-cli
npm install --global gulp-cli

# guard against preinstalled webstorm
brew install --cask webstorm --force

pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=webstorm
popd
