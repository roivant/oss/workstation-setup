
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing python"

brew install python

if ! [ -e ${HB_PATH}/bin/python ] && [ -e ${HB_PATH}/bin/python3 ]; then
    green "Symlinking ${HB_PATH}/bin/python3 to ${HB_PATH}/bin/python"
    ln -s ${HB_PATH}/bin/python3 ${HB_PATH}/bin/python
elif [ -e ${HB_PATH}/bin/python ]; then
    green "${HB_PATH}/bin/python exists"
    ls -l ${HB_PATH}/bin/python
fi

green "Installing Python tools"

brew install pyenv
brew install ipython
brew install xonsh

# Don't cask goland if toolbox is installed
if ! [ -e "/Applications/JetBrains Toolbox.app" ]; then
    brew install --cask pycharm
else
    yellow "Not installing Pycharm since you have the JetBrains Toolbox"
fi

pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=pycharm
popd
