
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing Ruby tools and latest Ruby"

brew install rbenv
if ! [ -e ~/.irbrc ]; then 
    cp ${WS_DIR}/files/.irbrc ~/.irbrc
fi
brew install readline
eval "$(rbenv init -)"
rbenv install $(rbenv install -l | grep -v - | tail -1) --skip-existing
rbenv global $(rbenv install -l | grep -v - | tail -1)
gem install bundler
rbenv rehash

# guard against pre-installed rubymine
brew install --cask rubymine --force

pushd ~/workspace/jetbrains-ide-prefs/cli
./bin/ide_prefs install --ide=rubymine
popd
