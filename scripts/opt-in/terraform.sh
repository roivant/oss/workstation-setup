
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -z ${WS_DIR+x} ]; then
    source "${MY_DIR}/../helpers/helpers.sh"
fi

green "Installing Terraform tooling"

brew tap hashicorp/tap
brew install hashicorp/tap/terraform

# shell completion

terraform -install-autocomplete