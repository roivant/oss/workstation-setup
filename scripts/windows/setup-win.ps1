Write-Host "
We're now going to install a variety of applications. This will take a minute, feel free to go grab some coffee.
"

Read-Host "[Hit Enter to continue]"

# Editors
choco install -y vim
choco install -y vscodium

# Browsers
choco install -y googlechrome
choco install -y firefox

# Communications
choco install -y slack
choco install -y signal
choco install -y zoom

# Utilities
choco install -y wget

# Code and frameworks
choco install -y python3
choco install -y nodejs
choco install -y vale
choco install -y postman

# Misc
choco install -y 1Password
choco install -y jq

$env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine")

bash scripts/common/vscodium.sh
bash scripts/common/git-aliases.sh
bash scripts/common/handbook.sh
