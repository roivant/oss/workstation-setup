
Write-Host "This script needs to run with local administrator rights.  
If you don't have local admin, you'll need to fix that first."

$workshere = Read-Host -Prompt 'Do you work for Roivant? [y/N]'
Switch ($workshere) {
    y { $isroiv = $true }
    default { $isroiv = $false }
}

if ($isroiv) {
    Write-Host @"

Welcome to...

########   #######  #### ##     ##    ###    ##    ## ########
##     ## ##     ##  ##  ##     ##   ## ##   ###   ##    ##
##     ## ##     ##  ##  ##     ##  ##   ##  ####  ##    ##
########  ##     ##  ##  ##     ## ##     ## ## ## ##    ##
##   ##   ##     ##  ##   ##   ##  ######### ##  ####    ##
##    ##  ##     ##  ##    ## ##   ##     ## ##   ###    ##
##     ##  #######  ####    ###    ##     ## ##    ##    ##

"@
}
else {
    Write-Host @"

Welcome to...

##      ##  #######  ########  ##    ##  ######  ########    ###    ######## ####  #######  ##    ## 
##  ##  ## ##     ## ##     ## ##   ##  ##    ##    ##      ## ##      ##     ##  ##     ## ###   ## 
##  ##  ## ##     ## ##     ## ##  ##   ##          ##     ##   ##     ##     ##  ##     ## ####  ## 
##  ##  ## ##     ## ########  #####     ######     ##    ##     ##    ##     ##  ##     ## ## ## ## 
##  ##  ## ##     ## ##   ##   ##  ##         ##    ##    #########    ##     ##  ##     ## ##  #### 
##  ##  ## ##     ## ##    ##  ##   ##  ##    ##    ##    ##     ##    ##     ##  ##     ## ##   ### 
 ###  ###   #######  ##     ## ##    ##  ######     ##    ##     ##    ##    ####  #######  ##    ## 
          ######  ######## ######## ##     ## ########                                               
         ##    ## ##          ##    ##     ## ##     ##                                              
         ##       ##          ##    ##     ## ##     ##                                              
          ######  ######      ##    ##     ## ########                                               
               ## ##          ##    ##     ## ##                                                     
         ##    ## ##          ##    ##     ## ##                                                     
          ######  ########    ##     #######  ##       
 
"@
}

Write-Host "This is a small program that will add some software, apply 
some common settings and generally get your machine set up.  
We'll ask you a few questions, and walk you through a few 
setup steps.  It will take a few minutes to complete, but 
you only need to watch this window for the first few steps, 
then it'll just do its thing"

Write-Host "[Hit Enter to continue]" -ForegroundColor Yellow
Read-Host

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

Write-Host "Installing base dependencies - Git and Python" -ForegroundColor Green
choco install -y git python --params "/GitAndUnixToolsOnPath"

Start-Sleep -Seconds 2.5

$env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine")

Write-Host "Refreshing environment after installs"
refreshenv

Write-Host "Creating workspace directory, if it doesn't exist"
New-Item -ItemType Directory -Force -Path $HOME\workspace

$gitlabssh = Read-Host -Prompt 'Do you you need to set up SSH keys for GitLab? [y/N] '
Switch ($gitlabssh) {
    y { $dossh = $true }
    default { $dossh = $false }
}

if ($dossh) {

    Write-Host "Next we're going to set up some access to GitLab. 
    We'll open a browser tab for you. Log in to Okta as needed, 
    if you are prompted to do so."
    Read-Host "[Hit Enter to continue]"

    Start-Process "https://roivant.okta.com/home/roivant_gitlabsaml_1/0oahjpu8c3Gcy4m801t7/alnhjpyn235Bd55471t7"

    Write-Host "Once you're logged in, [Hit Enter to continue]" -ForegroundColor Yellow
    Read-Host

    echo "
    Now we'll open a tab directly to your SSH settings in GitLab."

    Start-Process "https://gitlab.com/-/profile/keys"

    Write-Host "Ensuring the .ssh directory exists"
    New-Item -ItemType Directory -Force -Path ~\.ssh

    Write-Host "Generating an SSH key"
    $iam = $env:UserName -replace " ", "_"
    $keyfile = "$HOME\.ssh\id_ed25519"

    #If the key file does not exist, generate a new key.
    if (-not(Test-Path -Path $keyfile -PathType Leaf)) {
        try {
            # known_host is having issues on windows
            # ssh-keyscan -H gitlab.com >> $HOME/.ssh/known_hosts
            ssh-keygen -t ed25519 -N '""' -C "$iam@roivant" -f $keyfile
        }
        catch {
            throw $_.Exception.Message
        }
    }
    # If the file already exists, show the message and do nothing.
    else {
        Write-Host "Key already exists, yay!"
    }

    Write-Host "
    Copy all the text in between the lines below, and in the last GitLab 
    page we opened, paste it into the large text area that says 
    'Typically starts with...'.
    "

    Write-Host "
    ############
    NOTE: If you're re-running this app and you've already added your 
    SSH key to GitLab, you can SKIP THIS STEP
    ############
    "

    Write-Host "#### copy the text in between this line #####"
    type "$keyfile.pub"
    Write-Host "#### and this line #####"

    Write-Host "
    After you paste the key into the text field, **Leave 'Expires at' blank** 
    and click 'Add key'."

    Write-Host "[Hit Enter to continue]" -ForegroundColor Yellow
    Read-Host

}

Write-Host "Checking for the Workstation Setup repository"

Set-Location $HOME\workspace

# known_hosts is having problems on Windows. Set this here so we can clone
Set-Item -Path Env:GIT_SSH_COMMAND -Value "ssh -o StrictHostKeyChecking=no"

# If the key file does not exist, generate a new key.
if (-not(Test-Path -Path $HOME\workspace\workstation-setup)) {
    try {
        Write-Host "Cloning workstation-setup"
        git clone git@gitlab.com:roivant/oss/workstation-setup.git
        cd workstation-setup
    }
    catch {
        throw $_.Exception.Message
    }
}
else {
    Write-Host "Updating workstation-setup"
}

Set-Location $HOME\workspace\workstation-setup
git pull

invoke-expression -Command .\scripts\windows\setup-win.ps1

Write-Host "[Hit Enter to exit]" -ForegroundColor Yellow
Read-Host

