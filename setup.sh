#!/usr/bin/env bash

GITLAB_SSO_URL="https://roivant.okta.com/home/roivant_gitlabsaml_1/0oahjpu8c3Gcy4m801t7/alnhjpyn235Bd55471t7"
WS_SETUP_URL="https://gitlab.com/roivant/oss/workstation-setup.git"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -e "${SCRIPT_DIR}/scripts/helpers/helpers.sh" ]; then
    source "${SCRIPT_DIR}/scripts/helpers/helpers.sh"
else
    tmpf=`mktemp`
    curl -fsSL https://gitlab.com/roivant/oss/workstation-setup/-/raw/main/scripts/helpers/helpers.sh > $tmpf
    source $tmpf
fi

redb "Hi there.  Do you work for Roivant? ('no' is fine) [y/N]:  " nobreaks
read workshere

case $workshere in
    y|Y|yes|Yes)
      isroi=1
      ;;
    *)
      isroi=''
      ;;
esac

output "Welcome to..."

if [[ $isroi -eq 1 ]]; then
purple '
██████╗  ██████╗ ██╗██╗   ██╗ █████╗ ███╗   ██╗████████╗
██╔══██╗██╔═══██╗██║██║   ██║██╔══██╗████╗  ██║╚══██╔══╝
██████╔╝██║   ██║██║██║   ██║███████║██╔██╗ ██║   ██║
██╔══██╗██║   ██║██║╚██╗ ██╔╝██╔══██║██║╚██╗██║   ██║
██║  ██║╚██████╔╝██║ ╚████╔╝ ██║  ██║██║ ╚████║   ██║
╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝  ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝
'

yellow "############
You said yes to Roivant. If you don't actually work here, 
you won't get anything interesting except lots of errors.
This script will mostly still work though.
############"

else
purple '
██╗    ██╗ ██████╗ ██████╗ ██╗  ██╗███████╗████████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
██║    ██║██╔═══██╗██╔══██╗██║ ██╔╝██╔════╝╚══██╔══╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
██║ █╗ ██║██║   ██║██████╔╝█████╔╝ ███████╗   ██║   ███████║   ██║   ██║██║   ██║██╔██╗ ██║
██║███╗██║██║   ██║██╔══██╗██╔═██╗ ╚════██║   ██║   ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
╚███╔███╔╝╚██████╔╝██║  ██║██║  ██╗███████║   ██║   ██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
 ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
                                                                                           
                        ███████╗███████╗████████╗██╗   ██╗██████╗                                      
                        ██╔════╝██╔════╝╚══██╔══╝██║   ██║██╔══██╗                                     
                        ███████╗█████╗     ██║   ██║   ██║██████╔╝                                     
                        ╚════██║██╔══╝     ██║   ██║   ██║██╔═══╝                                      
                        ███████║███████╗   ██║   ╚██████╔╝██║                                          
                        ╚══════╝╚══════╝   ╚═╝    ╚═════╝ ╚═╝                                          
'
fi

output "This is a small program that will add some software, apply some common 
settings and generally get your machine set up.  We'll ask you a few questions, 
and walk you through a few setup steps.  It will take a few minutes to complete, 
but you only need to watch this window for the first few steps, then it'll just
do its thing"

redb "############
Just a word of warning - this app is fairly opinionated.  It *will* change 
some of your settings, it *will* modify your Dock, it will add some apps to 
your menu bar.  All of it can be put back if you don't like it, but we 
recommend giving the setup a shot.
############"

blue "[Hit Enter to continue]" nobreaks
read

output "To start off, we're going to install some macOS components.  You'll 
get a popup confirming the install.  Just follow the prompts.  This may take
a few minutes the first time you run it, and you'll want to ensure you're on 
a reasonably fast internet connection."

yellow "
############
If you see an error here saying" nobreaks

yellowb "command line tools are already installed" 

yellow "that's good and OK, just keep going.
############
" nobreaks
blue "[Hit enter to continue]" nobreaks
read

xcodepath=`xcode-select -p`
if ! ([ "$xcodepath" == "/Library/Developer/CommandLineTools" ] ||
      [ "$xcodepath" == "/Applications/Xcode.app/Contents/Developer" ]); then

    xcode-select --install

    output "For any dialogs that pop up, just hit next/ok to proceed with install.  
    When the installation finishes,
    "

    blue "[Hit enter to continue]"
    read
else
green "Xcode looks good"
fi

output "
Do you need to set up GitLab SSH access? [Y/n]: " nobreaks
read dogitlab

case $dogitlab in
    n|N|No|NO|no)
        echo
        ;;
    *)
        output "Next we're going to set up access to GitLab. We'll open a browser tab for you. 
Log in as needed, if you are prompted to do so.  After you're logged 
into GitLab, come back to this window.
"

        blue "[Hit enter to continue]" nobreaks
        read

        open $GITLAB_SSO_URL

        output "Now we'll open a tab directly to your SSH settings in GitLab."

        blue "[Hit enter to continue]" nobreaks
        read

        open https://gitlab.com/-/profile/keys

        if ! [ -e ~/.ssh/id_ed25519.pub ]; then
            green "Creating an SSH key"
            ssh-keygen -t ed25519 -N "" -C "`whoami`@roivant" \
                -f ~/.ssh/id_ed25519 2>&1 >/dev/null
        fi

        output "You're going to copy all the text in between the lines below, and in the last GitLab page 
we opened, paste it into the large text area that says \"Typically starts with...\"."

        yellow "############
NOTE: If you're re-running this app and you've already added your SSH key to GitLab, 
      you can SKIP THIS STEP
############
"

        output "#### copy the red text in between this line #####" nobreaks
        redb "`cat ~/.ssh/id_ed25519.pub`" nobreaks
        output "#### and this line #####" nobreaks

        output "After you paste the key into the text field
Leave \"Expires at\" blank
and click \"Add key\".
"

        blue "[Hit enter to continue]" nobreaks
        read
        ;;
esac

green "Setting up your workspace folder"
mkdir -p ~/workspace

if [ -e ~/workspace/workstation-setup ]; then
    green "Updating Workstation Setup"
    cd ~/workspace/workstation-setup
    git pull
else
    green "Downloading Workstation Setup"
    cd ~/workspace
    git clone ${WS_SETUP_URL}
    cd ~/workspace/workstation-setup
fi

source "scripts/mac/setup-macos.sh"
